

class Book
  LITTLE_WORDS = ["and", "the", "a", "an", "in", "of"]

  attr_reader :title

  def title=(title)
    words = title.split(" ").map(&:downcase)
    new_words = words.map.with_index { |word, i|
      if LITTLE_WORDS.include?(word) && i != 0
        word
      else
        word.capitalize
      end
    }
    @title = new_words.join(" ")
  end
end
