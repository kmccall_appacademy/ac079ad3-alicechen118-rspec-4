class Timer
  attr_accessor :seconds
  def initialize(seconds=0)
    @seconds = seconds
  end

  def padded(num)
    if num > 10
      "#{num}"
    else
      "0#{num}"
    end
  end

  def timer_seconds
    ((seconds % 60)).to_i
  end

  def minutes
    ((seconds % 3600) / 60).to_i
  end

  def hours
    (seconds / 3600).to_i
  end

  def time_string
    "#{padded(hours)}:#{padded(minutes)}:#{padded(timer_seconds)}"
  end
end
