class Temperature

  attr_accessor :fahrenheit, :celsius

  def initialize(options)
    @fahrenheit = options[:f]
    @celsius = options[:c]

  end

  def in_fahrenheit
    return @fahrenheit if @celsius.nil?
    ctof(@celsius)
  end

  def in_celsius
    return @celsius if @fahrenheit.nil?
    ftoc(@fahrenheit)
  end

  def self.from_celsius(degree)
    Temperature.new(:c => degree)
  end

  def self.from_fahrenheit(degree)
    Temperature.new(:f => degree)
  end

  def ftoc(degree)
    (degree - 32) * (5.0 / 9)
  end

  def ctof(degree)
    (degree * (9.0 / 5)) + 32
  end

end

class Celsius < Temperature

  def initialize(c)
    @celsius = c
  end

end

class Fahrenheit < Temperature

  def initialize(f)
    @fahrenheit = f
  end


end
