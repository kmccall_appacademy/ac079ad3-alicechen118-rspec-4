class Dictionary

  attr_accessor :entries

  def initialize
    @entries = {}
  end

  def keywords
    @entries.keys.sort
  end

  def add(new_entries)
    @entries[new_entries] = nil if new_entries.is_a? String
    @entries.merge!(new_entries) if new_entries.is_a? Hash
  end

  def include?(keyword)
    keywords.any? { |el| el == keyword }
  end

  def find(entry)
    @entries.select do |keyword|
      keyword.include?(entry)
    end
  end

  def printable
    output = keywords.map do |keyword|
      %Q{[#{keyword}] "#{entries[keyword]}"}
    end
    output.join("\n")
  end

end
